package com.example;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class SampleTest {

    private Sample sample;

    @Before
    public void init() {
        sample = new Sample();
    }

    @Test
    public void testAdd() {assertEquals(3, sample.add(1, 2));}
    @Test
    public void testTrue() {assertTrue(sample.checkboolean(0));}
    @Test
    public void testFalse() {assertFalse(sample.checkboolean(1));}

}


